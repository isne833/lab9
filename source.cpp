#include <iostream>
#include "list.h"

using namespace std;
void main()
{
	List list;
	int f; // input the number you want to find
	
	//head push
	list.headPush(5);
	list.headPush(4);
	list.headPush(3);
	
	//tail push
	list.tailPush(1);
	list.tailPush(2);
	list.tailPush(3);
	
	list.display();
	cout << "\nHead Pop: ";
	cout<<list.headPop();
	cout << "\nTail Pop: ";
	cout << list.tailPop();
	list.deleteNode(5);
	cout << "\nAfter delete node list: ";
	list.display();
	cout << "\nEnter find number: ";
	cin >> f;
	if (list.isInList(f)) {
		cout << "Found";
	}
	else {
		cout << "Not Found";
	}

	
}
