#include <iostream>
#include "list.h"
using namespace std;

List::~List() {
	for (Node* p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}

void List::display() {//display function
	Node* tmp = new Node(0, 0);
	tmp = head;
	while (tmp != NULL)
	{
		cout << tmp->info << " ";
		tmp = tmp->next;
	}
}

void List::headPush(int n) { //head push function
	Node* p = new Node(n);
	if (isEmpty()) 
	{
		head = tail = p;
	}
	else
	{
		p->next = head;
		head = p;
	}
	tail->next = NULL;
}

void List::tailPush(int n) { //tail push function
	Node* tmp = new Node(n);
	if (isEmpty()) 
	{
		head = tail = tmp;
	}
	else
	{
		tail->next = tmp;
		tail = tmp;
	}
	tail->next = NULL;
}

int List::headPop() { //head pop function
	int tmp = head->info;
	Node* p = head;
	head = head->next;
	delete p;
	return tmp;
}

int List::tailPop() { //tail pop function
	int tmp = tail->info;
	Node* p = head;
	while (p->next != tail) 
	{
		p = p->next;
	}
	tail = p;
	p = p->next;
	delete p;
	tail->next = NULL;
	return tmp;
}

void List::deleteNode(int n) { //delete function
	Node* cur;
	Node* tmp;
	cur = head;
	tmp = cur;
	Node* p = head;
	while (cur->info != n) 
	{
		tmp = cur;
		cur = cur->next;
		if (cur == NULL) 
		{
			return;
		}
	}
	tmp->next = cur->next;
	delete cur;
}

bool List::isInList(int n) { //fund list function
	Node* p = head;
	while (p->info != n)
	{
		if (p->next == NULL) 
		{
			return false;
		}
		p = p->next;
	}
	return true;
}
